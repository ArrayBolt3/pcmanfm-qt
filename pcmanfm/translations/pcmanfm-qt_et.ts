<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="et">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../about.ui" line="14"/>
        <source>About</source>
        <translation>Rakenduse teave</translation>
    </message>
    <message>
        <location filename="../about.ui" line="25"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:16pt; font-weight:600;&quot;&gt;PCManFM-Qt&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../about.ui" line="48"/>
        <source>Lightweight file manager</source>
        <translation>Ressursisäästlik failihaldur</translation>
    </message>
    <message>
        <location filename="../about.ui" line="91"/>
        <source>Programming:
* Hong Jen Yee (PCMan) &lt;pcman.tw@gmail.com&gt;
</source>
        <translation>Programmeerija:
* Hong Jen Yee (PCMan) &lt;pcman.tw@gmail.com&gt;
</translation>
    </message>
    <message>
        <location filename="../about.ui" line="113"/>
        <source>PCManFM-Qt File Manager

Copyright (C) 2009 - 2014 洪任諭 (Hong Jen Yee)

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.</source>
        <translation>PCManFM-Qt failihaldur

Copyright (C) 2009 - 2014 洪任諭 (Hong Jen Yee)

See programm on tasuta tarkvara; seda võib levitada ja/või
muuta Vaba Tarkvara Sihtasutuse avaldatud 
GNU Üldise Avaliku Litsentsi tingimuste kohaselt;
kas 2.versioon või hilisem (teie valikul).

Seda programmi levitatakse lootuses, et see on kasulik,
kuid ilma mingi GARANTIITA; isegi ilma
KAUBANDUSLIKU VÕI KONKREETSEKS EESMÄRGIKS VASTAVUSE
garantiita. Lisateabe saamiseks vaata GNU Üldist Avalikku Litsentsi.

Selle programmiga on kaasas GNU Üldise Avaliku Litsentsi koopia;
kui mitte, kirjuta Vaba Tarkvara Sihtasutuse aadressile
Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.</translation>
    </message>
    <message>
        <location filename="../about.ui" line="82"/>
        <source>Authors</source>
        <translation>Autorid</translation>
    </message>
    <message>
        <location filename="../about.ui" line="58"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;a href=&quot;https://github.com/lxqt/pcmanfm-qt&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;https://github.com/lxqt/pcmanfm-qt&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;
</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;a href=&quot;https://github.com/lxqt/pcmanfm-qt&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;https://github.com/lxqt/pcmanfm-qt&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;
</translation>
    </message>
    <message>
        <location filename="../about.ui" line="104"/>
        <source>License</source>
        <translation>Litsents</translation>
    </message>
</context>
<context>
    <name>AutoRunDialog</name>
    <message>
        <location filename="../autorun.ui" line="14"/>
        <source>Removable medium is inserted</source>
        <translation>Sisestati eemaldatav andmekandja</translation>
    </message>
    <message>
        <location filename="../autorun.ui" line="33"/>
        <source>&lt;b&gt;Removable medium is inserted&lt;/b&gt;</source>
        <translation>&lt;b&gt;Sisestati eemaldatav andmekandja&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../autorun.ui" line="40"/>
        <source>Type of medium:</source>
        <translation>Andmekandja tüüp:</translation>
    </message>
    <message>
        <location filename="../autorun.ui" line="47"/>
        <source>Detecting...</source>
        <translation>Tuvastamine...</translation>
    </message>
    <message>
        <location filename="../autorun.ui" line="56"/>
        <source>Please select the action you want to perform:</source>
        <translation>Sisesta soovitud tegevus:</translation>
    </message>
</context>
<context>
    <name>BulkRenameDialog</name>
    <message>
        <location filename="../bulk-rename.ui" line="6"/>
        <source>Bulk Rename</source>
        <translation>Muuda paljude failide nimesid</translation>
    </message>
    <message>
        <location filename="../bulk-rename.ui" line="48"/>
        <source># will be replaced by numbers starting with:</source>
        <translation># asendatakse numbritega alates:</translation>
    </message>
    <message>
        <location filename="../bulk-rename.ui" line="71"/>
        <source>Rename selected files to:</source>
        <translation>Nimeta valitud failid ümber:</translation>
    </message>
    <message>
        <location filename="../bulk-rename.ui" line="84"/>
        <source>Name#</source>
        <translation>Nimi#</translation>
    </message>
</context>
<context>
    <name>ConnectServerDialog</name>
    <message>
        <location filename="../connect.ui" line="14"/>
        <source>Connect to remote server</source>
        <translation>Ühendu kaugserveriga</translation>
    </message>
    <message>
        <location filename="../connect.ui" line="23"/>
        <source>Anonymous &amp;login</source>
        <translation>Anonüümne &amp;sisselogimine</translation>
    </message>
    <message>
        <location filename="../connect.ui" line="36"/>
        <source>Login as &amp;user:</source>
        <translation>Logi sisse &amp;kasutajana:</translation>
    </message>
    <message>
        <location filename="../connect.ui" line="65"/>
        <source>Specify remote folder to connect</source>
        <translation>Määra ühendamiseks kaugkataloog</translation>
    </message>
    <message>
        <location filename="../connect.ui" line="72"/>
        <source>Type:</source>
        <translation>Tüüp:</translation>
    </message>
    <message>
        <location filename="../connect.ui" line="79"/>
        <source>Port:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../connect.ui" line="86"/>
        <source>Path:</source>
        <translation>Rada:</translation>
    </message>
    <message>
        <location filename="../connect.ui" line="96"/>
        <source>Host:</source>
        <translation>Masin:</translation>
    </message>
</context>
<context>
    <name>DesktopEntryDialog</name>
    <message>
        <location filename="../desktopentrydialog.ui" line="14"/>
        <source>Create Launcher</source>
        <translation>Loo käivitaja</translation>
    </message>
    <message>
        <location filename="../desktopentrydialog.ui" line="38"/>
        <source>Name:</source>
        <translation>Nimi:</translation>
    </message>
    <message>
        <location filename="../desktopentrydialog.ui" line="55"/>
        <source>Description:</source>
        <translation>Kirjeldus:</translation>
    </message>
    <message>
        <location filename="../desktopentrydialog.ui" line="69"/>
        <source>Comment:</source>
        <translation>Kommentaar:</translation>
    </message>
    <message>
        <location filename="../desktopentrydialog.ui" line="83"/>
        <source>Command:</source>
        <translation>Käsk:</translation>
    </message>
    <message>
        <location filename="../desktopentrydialog.ui" line="99"/>
        <location filename="../desktopentrydialog.ui" line="127"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../desktopentrydialog.ui" line="108"/>
        <source>Icon:</source>
        <translation>Ikoon:</translation>
    </message>
    <message>
        <location filename="../desktopentrydialog.ui" line="136"/>
        <location filename="../desktopentrydialog.ui" line="146"/>
        <source>Run in terminal?</source>
        <translation>Kas käivitada terminalis?</translation>
    </message>
    <message>
        <location filename="../desktopentrydialog.ui" line="139"/>
        <source>Terminal:</source>
        <translation>Terminal:</translation>
    </message>
    <message>
        <location filename="../desktopentrydialog.ui" line="150"/>
        <source>No</source>
        <translation>Ei</translation>
    </message>
    <message>
        <location filename="../desktopentrydialog.ui" line="155"/>
        <source>Yes</source>
        <translation>Jah</translation>
    </message>
    <message>
        <location filename="../desktopentrydialog.ui" line="163"/>
        <source>Type:</source>
        <translation>Tüüp:</translation>
    </message>
    <message>
        <location filename="../desktopentrydialog.ui" line="171"/>
        <source>Application</source>
        <translation>Rakendus</translation>
    </message>
    <message>
        <location filename="../desktopentrydialog.ui" line="176"/>
        <source>Link</source>
        <translation>Link</translation>
    </message>
</context>
<context>
    <name>DesktopFolder</name>
    <message>
        <location filename="../desktop-folder.ui" line="14"/>
        <source>Form</source>
        <translation>Vorm</translation>
    </message>
    <message>
        <location filename="../desktop-folder.ui" line="23"/>
        <source>Desktop</source>
        <translation>Töölaud</translation>
    </message>
    <message>
        <location filename="../desktop-folder.ui" line="29"/>
        <source>Desktop folder:</source>
        <translation>Töölauakaust:</translation>
    </message>
    <message>
        <location filename="../desktop-folder.ui" line="36"/>
        <source>Image file</source>
        <translation>Pildifail</translation>
    </message>
    <message>
        <location filename="../desktop-folder.ui" line="42"/>
        <source>Folder path</source>
        <translation>Kausta asukoht</translation>
    </message>
    <message>
        <location filename="../desktop-folder.ui" line="49"/>
        <source>&amp;Browse</source>
        <translation>&amp;Sirvi</translation>
    </message>
</context>
<context>
    <name>DesktopPreferencesDialog</name>
    <message>
        <location filename="../desktop-preferences.ui" line="14"/>
        <source>Desktop Preferences</source>
        <translation>Töölaua eelistused</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="247"/>
        <location filename="../desktop-preferences.ui" line="253"/>
        <source>Background</source>
        <translation>Taust</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="294"/>
        <source>Wallpaper mode:</source>
        <translation>Taustapildi režiim:</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="317"/>
        <source>Wallpaper image file:</source>
        <translation>Taustapildi pildifail:</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="259"/>
        <source>Select background color:</source>
        <translation>Vali taustavärv:</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="326"/>
        <source>Image file</source>
        <translation>Pildifail</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="332"/>
        <source>Image file path</source>
        <translation>Pildifaili rada</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="339"/>
        <source>&amp;Browse</source>
        <translation>&amp;Sirvi</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="30"/>
        <source>Icons</source>
        <translation>Ikoonid</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="36"/>
        <source>Icon size:</source>
        <translation>Ikooni suurus:</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="49"/>
        <source>Label Text</source>
        <translation>Sildi tekst</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="113"/>
        <source>Select shadow color:</source>
        <translation>Vali taustavärv:</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="61"/>
        <source>Select font:</source>
        <translation>Vali font:</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="24"/>
        <source>General</source>
        <translation>Üldine</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="87"/>
        <source>Select text color:</source>
        <translation>Vali teksti värv:</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="136"/>
        <source>Spacing</source>
        <translation>Vahemaa</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="142"/>
        <source>Minimum item margins:</source>
        <translation>Vähim vahemaa:</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="149"/>
        <source>3 px by default.</source>
        <translation>Vaikimisi 3 px.</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="152"/>
        <location filename="../desktop-preferences.ui" line="176"/>
        <source> px</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="165"/>
        <source>x</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="172"/>
        <source>1 px by default.
A space is also reserved for 3 lines of text.</source>
        <translation>Vaikimisi 1 px.
Ruumi varuti ka 3 tekstirea jaoks.</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="189"/>
        <source>Lock</source>
        <translation>Lukusta</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="212"/>
        <source>By default, desktop folders will be opened in PCManFM-Qt if they
are left clicked, even when it is not the default file manager.</source>
        <translation>Vaikimisi avatakse töölaua kaustad PCManFM-Qt-s, kui need
on olemas klõpsatakse vasakul, isegi kui see pole vaikefailihaldur.</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="216"/>
        <source>Open desktop folders in default file manager by left clicking</source>
        <translation>Avage vaikefailihalduris töölaua kaustad, klõpsates vasakul</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="223"/>
        <source>Make all items stick to their positions</source>
        <translation>Muuda kõik kirjed oma asukohas kleepuvaks</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="282"/>
        <source>Wallpaper</source>
        <translation>Taustapilt</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="348"/>
        <source>Transform image based on EXIF data</source>
        <translation>Teisenda pilt EXIF-andmete põhjal</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="355"/>
        <source>Individual wallpaper for each monitor</source>
        <translation>Iga monitori jaoks eraldi tapeet</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="382"/>
        <source>Slide Show</source>
        <translation>Slaidiseanss</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="388"/>
        <source>Enable Slide Show</source>
        <translation>Luba slaidiseanss</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="400"/>
        <source>Wallpaper image folder:</source>
        <translation>Taustapildi kaust:</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="407"/>
        <source>Browse</source>
        <translation>Sirvi</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="414"/>
        <source> hour(s)</source>
        <translation> tund(i)</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="424"/>
        <source>and</source>
        <translation>ja</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="437"/>
        <source>Intervals less than 5min will be ignored</source>
        <translation>Alla 5 min intervalle eiratakse</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="440"/>
        <source>Interval:</source>
        <translation>Intervall:</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="447"/>
        <source> minute(s)</source>
        <translation> minut(it)</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="473"/>
        <source>Wallpaper folder</source>
        <translation>Taustapildi kaust</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="496"/>
        <source>Randomize the slide show</source>
        <translation>Juhuslik slaidiseanss</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="529"/>
        <source>Visible Shortcuts</source>
        <translation>Nähtavad otseteed</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="535"/>
        <source>Home</source>
        <translation>Avaleht</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="546"/>
        <source>Trash</source>
        <translation>Prügikast</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="557"/>
        <source>Computer</source>
        <translation>Arvuti</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="568"/>
        <source>Network</source>
        <translation>Võrk</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="523"/>
        <source>Advanced</source>
        <translation>Täpsem</translation>
    </message>
</context>
<context>
    <name>HiddenShortcutsDialog</name>
    <message>
        <location filename="../shortcuts.ui" line="14"/>
        <source>Hidden Shortcuts</source>
        <translation>Peidetud kiirklahvid</translation>
    </message>
    <message>
        <location filename="../shortcuts.ui" line="21"/>
        <source>Shortcut</source>
        <translation>Kiirklahv</translation>
    </message>
    <message>
        <location filename="../shortcuts.ui" line="26"/>
        <source>Action</source>
        <translation>Tegevus</translation>
    </message>
    <message>
        <location filename="../shortcuts.ui" line="31"/>
        <source>Esc</source>
        <translation>Esc</translation>
    </message>
    <message>
        <location filename="../shortcuts.ui" line="34"/>
        <source>Focus view, clear filter bar</source>
        <translation>Tühjenda ja eemalda filtririba</translation>
    </message>
    <message>
        <location filename="../shortcuts.ui" line="39"/>
        <source>Ctrl+Esc</source>
        <translation>Ctrl+Esc</translation>
    </message>
    <message>
        <location filename="../shortcuts.ui" line="42"/>
        <source>Focus side-pane</source>
        <translation>Säti fookus külgpaneelile</translation>
    </message>
    <message>
        <location filename="../shortcuts.ui" line="47"/>
        <source>Ctrl+L</source>
        <translation>Ctrl+L</translation>
    </message>
    <message>
        <location filename="../shortcuts.ui" line="50"/>
        <location filename="../shortcuts.ui" line="58"/>
        <source>Focus path entry</source>
        <translation>Säti fookus asukoharibale</translation>
    </message>
    <message>
        <location filename="../shortcuts.ui" line="55"/>
        <source>Alt+D</source>
        <translation>Alt+D</translation>
    </message>
    <message>
        <location filename="../shortcuts.ui" line="63"/>
        <source>Ctrl+Tab</source>
        <translation>Ctrl+Tab</translation>
    </message>
    <message>
        <location filename="../shortcuts.ui" line="66"/>
        <location filename="../shortcuts.ui" line="82"/>
        <source>Next tab</source>
        <translation>Järgmine kaart</translation>
    </message>
    <message>
        <location filename="../shortcuts.ui" line="71"/>
        <source>Ctrl+Shift+Tab</source>
        <translation>Ctrl+Shift+Tab</translation>
    </message>
    <message>
        <location filename="../shortcuts.ui" line="74"/>
        <location filename="../shortcuts.ui" line="90"/>
        <source>Previous tab</source>
        <translation>Eelmine kaart</translation>
    </message>
    <message>
        <location filename="../shortcuts.ui" line="79"/>
        <source>Ctrl+PageDown</source>
        <translation>Ctrl+PageDown</translation>
    </message>
    <message>
        <location filename="../shortcuts.ui" line="87"/>
        <source>Ctrl+PageUp</source>
        <translation>Ctrl+PageUp</translation>
    </message>
    <message>
        <location filename="../shortcuts.ui" line="95"/>
        <source>Ctrl+Number</source>
        <translation>Ctrl+Number</translation>
    </message>
    <message>
        <location filename="../shortcuts.ui" line="98"/>
        <location filename="../shortcuts.ui" line="106"/>
        <source>Jump to tab</source>
        <translation>Säti fookus kaardile</translation>
    </message>
    <message>
        <location filename="../shortcuts.ui" line="103"/>
        <source>Alt+Number</source>
        <translation>Alt+Number</translation>
    </message>
    <message>
        <location filename="../shortcuts.ui" line="111"/>
        <source>Backspace</source>
        <translation>Tagasisammuklahv</translation>
    </message>
    <message>
        <location filename="../shortcuts.ui" line="114"/>
        <source>Go up</source>
        <translation>Liigu üles</translation>
    </message>
    <message>
        <location filename="../shortcuts.ui" line="119"/>
        <source>Ctrl+F</source>
        <translation>Ctrl+F</translation>
    </message>
    <message>
        <location filename="../shortcuts.ui" line="122"/>
        <source>Search dialog</source>
        <translation>Otsinguvaade</translation>
    </message>
    <message>
        <location filename="../shortcuts.ui" line="127"/>
        <source>Shift+Insert</source>
        <translation>Shift+Insert</translation>
    </message>
    <message>
        <location filename="../shortcuts.ui" line="130"/>
        <source>Paste into transient filter bar</source>
        <translation>Aseta ajutisele filtriribale</translation>
    </message>
    <message>
        <location filename="../shortcuts.ui" line="135"/>
        <source>Drag+Shift</source>
        <translation>Drag+Shift</translation>
    </message>
    <message>
        <location filename="../shortcuts.ui" line="138"/>
        <source>Move file on dropping</source>
        <translation>Lohistamisel tõsta fail uude kohta</translation>
    </message>
    <message>
        <location filename="../shortcuts.ui" line="143"/>
        <source>Drag+Ctrl</source>
        <translation>Drag+Ctrl</translation>
    </message>
    <message>
        <location filename="../shortcuts.ui" line="146"/>
        <source>Copy file on dropping</source>
        <translation>Lohistamisel kopeeri fail uude kohta</translation>
    </message>
    <message>
        <location filename="../shortcuts.ui" line="151"/>
        <source>Drag+Shift+Ctrl</source>
        <translation>Drag+Shift+Ctrl</translation>
    </message>
    <message>
        <location filename="../shortcuts.ui" line="154"/>
        <source>Make a symlink on dropping</source>
        <translation>Lohistamisel tee failist sümbolviide</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../main-win.ui" line="14"/>
        <source>File Manager</source>
        <translation>Failihaldur</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="133"/>
        <source>&amp;Toolbars</source>
        <translation>&amp;Tööriistaribad</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="140"/>
        <source>Path &amp;Bar</source>
        <translation>Asukoha&amp;riba</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="147"/>
        <source>&amp;Filtering</source>
        <translation>&amp;Filtreerimine</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="221"/>
        <source>&amp;Tools</source>
        <translation>&amp;Tööriistad</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="280"/>
        <source>Go &amp;Up</source>
        <translation>Liigu &amp;üles</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="283"/>
        <source>Go Up</source>
        <translation>Liigu üles</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="286"/>
        <source>Alt+Up</source>
        <translation>Alt+Up</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="295"/>
        <source>&amp;Home</source>
        <translation>&amp;Avaleht</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="298"/>
        <source>Alt+Home</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="307"/>
        <source>&amp;Reload</source>
        <translation>&amp;Laadi uuesti</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="310"/>
        <source>F5</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="319"/>
        <source>Go</source>
        <translation>Liigu</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="328"/>
        <source>Quit</source>
        <translation>Sulge</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="337"/>
        <source>&amp;About</source>
        <translation>&amp;Rakenduse teave</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="346"/>
        <source>&amp;New Window</source>
        <translation>&amp;Uus aken</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="349"/>
        <source>New Window</source>
        <translation>Uus aken</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="352"/>
        <source>Ctrl+N</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="360"/>
        <source>Show &amp;Hidden</source>
        <translation>Kuva &amp;peidetud failid</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="363"/>
        <source>Ctrl+H</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="372"/>
        <source>&amp;Computer</source>
        <translation>&amp;Arvuti</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="381"/>
        <source>&amp;Trash</source>
        <translation>&amp;Prügikast</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="386"/>
        <source>&amp;Network</source>
        <translation>&amp;Võrk</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="395"/>
        <source>&amp;Desktop</source>
        <translation>&amp;Töölaud</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="404"/>
        <source>&amp;Add to Bookmarks</source>
        <translation>&amp;Lisa järjehoidjaks</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="409"/>
        <source>&amp;Applications</source>
        <translation>&amp;Rakendused</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="414"/>
        <source>Reload</source>
        <translation>Lae uuesti</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="422"/>
        <source>&amp;Icon View</source>
        <translation>&amp;Ikoonivaade</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="430"/>
        <source>&amp;Compact View</source>
        <translation>&amp;Kompaktne vaade</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="438"/>
        <source>&amp;Detailed List</source>
        <translation>&amp;Üksikasjalik nimekiri</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="446"/>
        <source>&amp;Thumbnail View</source>
        <translation>&amp;Pisipildivaade</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="455"/>
        <source>Cu&amp;t</source>
        <translation>L&amp;õika</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="458"/>
        <source>Ctrl+X</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="467"/>
        <source>&amp;Copy</source>
        <translation>&amp;Kopeeri</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="470"/>
        <source>Ctrl+C</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="479"/>
        <source>&amp;Paste</source>
        <translation>&amp;Aseta</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="482"/>
        <source>Ctrl+V</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="487"/>
        <source>Select &amp;All</source>
        <translation>Vali &amp;kõik</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="490"/>
        <source>Ctrl+A</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="495"/>
        <source>Pr&amp;eferences</source>
        <translation>Ee&amp;listused</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="503"/>
        <source>&amp;Ascending</source>
        <translation>&amp;Kasvav</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="511"/>
        <source>&amp;Descending</source>
        <translation>&amp;Kahanev</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="519"/>
        <source>&amp;By File Name</source>
        <translation>&amp;Faili nime järgi</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="527"/>
        <source>By &amp;Modification Time</source>
        <translation>&amp;Muutmise aja järgi</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="535"/>
        <source>By C&amp;reation Time</source>
        <translation>&amp;Loomisaja alusel</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="543"/>
        <source>By Deletio&amp;n Time</source>
        <translation>&amp;Kustutamise aja alusel</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="551"/>
        <source>By File &amp;Type</source>
        <translation>Faili &amp;tüübi järgi</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="559"/>
        <source>By &amp;Owner</source>
        <translation>&amp;Omaniku järgi</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="567"/>
        <source>By &amp;Group</source>
        <translation>&amp;Rühma alusel</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="575"/>
        <source>&amp;Folder First</source>
        <translation>&amp;Kaustad ees</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="583"/>
        <source>&amp;Hidden Last</source>
        <translation>&amp;Peidetud failid lõpus</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="948"/>
        <source>Preserve Settings Recursively from &amp;Here</source>
        <translation>Kasuta siit &amp;alates rekursiivselt samu seadistusi</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="953"/>
        <source>&amp;Go to Source of Inherited Settings</source>
        <translation>&amp;Mine rekursiivsete seadistuste algpunkti</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="958"/>
        <source>&amp;Remove Settings of Nonexistent Folders</source>
        <translation>&amp;Eemalda seadistused olematutest kaustadest</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="636"/>
        <source>Ctrl+Shift+A</source>
        <translation>Ctrl+Shift+A</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="685"/>
        <source>&amp;Case Sensitive</source>
        <translation>&amp;Tõstutundlik</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="693"/>
        <source>By File &amp;Size</source>
        <translation>Faili &amp;suuruse järgi</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="698"/>
        <source>&amp;Close Window</source>
        <translation>&amp;Sulge aken</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="701"/>
        <source>Ctrl+Q</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="719"/>
        <source>Open Tab in &amp;Root Instance</source>
        <translation>Ava kaart juu&amp;rkasutajana</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="733"/>
        <source>&amp;Folder</source>
        <translation>&amp;Kaust</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="745"/>
        <source>&amp;Blank File</source>
        <translation>&amp;Tühi fail</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="832"/>
        <source>Preserve Settings for &amp;This Folder</source>
        <translation>Säilita &amp;selle kausta seadistused</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="869"/>
        <source>&amp;Show/Focus Filter Bar</source>
        <translation>Ava filtririba ja &amp;säti fookus sinna</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="872"/>
        <source>Show Filter Bar</source>
        <translation>Kuva filtririba</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="875"/>
        <source>Ctrl+I</source>
        <translation>Ctrl+I</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="883"/>
        <source>S&amp;plit View</source>
        <translation>J&amp;aotatud vaade</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="886"/>
        <source>Split View</source>
        <translation>Poolitatud vaade</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="889"/>
        <source>F6</source>
        <translation>F6</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="894"/>
        <source>&amp;Copy Full Path</source>
        <translation>&amp;Kopeeri kogu rada</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="897"/>
        <source>Ctrl+Shift+C</source>
        <translation>Ctrl+Shift+C</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="908"/>
        <source>Show Thumb&amp;nails</source>
        <translation>&amp;Näita pisipilte</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="911"/>
        <source>Show Thumbnails</source>
        <translation>Näita pisipilte</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="919"/>
        <source>S&amp;ide Pane</source>
        <translation>&amp;Külgpaneel</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="922"/>
        <source>F9</source>
        <translation>F9</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="927"/>
        <source>Hidden &amp;Shortcuts</source>
        <translation>&amp;Peidetud kiirklahvid</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="932"/>
        <source>Open Tab in &amp;Admin Mode</source>
        <translation>Ava kaart &amp;administraatorina</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="937"/>
        <location filename="../main-win.ui" line="940"/>
        <source>Create Launcher</source>
        <translation>Loo käivitaja</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="767"/>
        <source>Ctrl+B</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="772"/>
        <source>&amp;Clear All Filters</source>
        <translation>&amp;Kustuta kõik filtrid</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="775"/>
        <source>Ctrl+Shift+K</source>
        <translation>Ctrl+Shift+K</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="784"/>
        <source>Close &amp;previous tabs</source>
        <translation>Sulge &amp;eelmised kaardid</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="793"/>
        <source>Close &amp;next tabs</source>
        <translation>Sulge &amp;järgmised kaardid</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="837"/>
        <source>Connect to &amp;Server</source>
        <translation>Ühenda &amp;serveriga</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="845"/>
        <source>&amp;Location</source>
        <translation>&amp;Asukoht</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="853"/>
        <source>&amp;Path Buttons</source>
        <translation>&amp;Asukoha nupud</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="858"/>
        <source>&amp;Bulk Rename</source>
        <translation>&amp;Muuda paljude failide nimesid</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="861"/>
        <source>Bulk Rename</source>
        <translation>Muuda paljude failide nimesid</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="864"/>
        <source>Ctrl+F2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="798"/>
        <source>Close &amp;other tabs</source>
        <translation>Sulge &amp;muud kaardid</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="764"/>
        <source>Permanent &amp;filter bar</source>
        <translation>Püsiv &amp;filtririba</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="806"/>
        <source>&amp;Menu bar</source>
        <translation>&amp;Menüüriba</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="809"/>
        <source>Menu bar</source>
        <translation>Menüüriba</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="812"/>
        <source>Ctrl+M</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="821"/>
        <location filename="../main-win.ui" line="824"/>
        <source>Menu</source>
        <translation>Menüü</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="592"/>
        <source>New &amp;Tab</source>
        <translation>Uus &amp;kaart</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="154"/>
        <source>&amp;Customized View Settings</source>
        <translation>Vaate &amp;kohandatud seadistused</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="595"/>
        <source>New Tab</source>
        <translation>Uus kaart</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="598"/>
        <source>Ctrl+T</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="607"/>
        <source>Go &amp;Back</source>
        <translation>Mine &amp;tagasi</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="610"/>
        <source>Go Back</source>
        <translation>Mine tagasi</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="613"/>
        <source>Alt+Left</source>
        <translation>Alt+Left</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="622"/>
        <source>Go &amp;Forward</source>
        <translation>Liigu &amp;edasi</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="625"/>
        <source>Go Forward</source>
        <translation>Liigu edasi</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="628"/>
        <source>Alt+Right</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="633"/>
        <source>&amp;Invert Selection</source>
        <translation>&amp;Pööra valik</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="645"/>
        <source>&amp;Delete</source>
        <translation>&amp;Kustuta</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="648"/>
        <source>Del</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="653"/>
        <source>&amp;Rename</source>
        <translation>&amp;Muuda nime</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="656"/>
        <source>F2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="661"/>
        <source>C&amp;lose Tab</source>
        <translation>S&amp;ulge kaart</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="664"/>
        <source>Ctrl+W</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="669"/>
        <source>File &amp;Properties</source>
        <translation>Faili &amp;omadused</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="672"/>
        <source>Alt+Return</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="677"/>
        <source>&amp;Folder Properties</source>
        <translation>&amp;Kausta omadused</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="706"/>
        <source>Edit Bookmarks</source>
        <translation>Muuda järjehoidjaid</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="711"/>
        <source>Open &amp;Terminal</source>
        <translation>Ava &amp;terminal</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="714"/>
        <source>F4</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="724"/>
        <source>&amp;Edit Bookmarks</source>
        <translation>&amp;Muuda järjehoidjaid</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="736"/>
        <source>Ctrl+Shift+N</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="748"/>
        <source>Ctrl+Alt+N</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="753"/>
        <source>&amp;Find Files</source>
        <translation>&amp;Otsi faile</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="756"/>
        <source>F3</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="70"/>
        <source>&amp;File</source>
        <translation>&amp;Fail</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="74"/>
        <source>C&amp;reate New</source>
        <translation>&amp;Loo uus</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="92"/>
        <source>&amp;Help</source>
        <translation>&amp;Abiteave</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="99"/>
        <location filename="../main-win.ui" line="123"/>
        <source>&amp;View</source>
        <translation>&amp;Vaata</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="103"/>
        <source>&amp;Sorting</source>
        <translation>&amp;Sortimine</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="179"/>
        <source>&amp;Edit</source>
        <translation>&amp;Muuda</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="197"/>
        <source>&amp;Bookmarks</source>
        <translation>&amp;Järjehoidjad</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="204"/>
        <source>&amp;Go</source>
        <translation>&amp;Mine</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="246"/>
        <source>Main Toolbar</source>
        <translation>Põhiline tööriistariba</translation>
    </message>
</context>
<context>
    <name>PCManFM::Application</name>
    <message>
        <location filename="../application.cpp" line="168"/>
        <source>Name of configuration profile</source>
        <translation>Seadistusprofiili nimi</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="168"/>
        <source>PROFILE</source>
        <translation>PROFIIL</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="171"/>
        <source>Run PCManFM-Qt as a daemon</source>
        <translation>Käivita PCManFM-Qt taustarakendusena</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="174"/>
        <source>Quit PCManFM-Qt</source>
        <translation>Välju PCManFM-Qt rakendusest</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="177"/>
        <source>Launch desktop manager</source>
        <translation>Käivita töölauahaldur</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="180"/>
        <source>Turn off desktop manager if it&apos;s running</source>
        <translation>Kui see töötab, lülita töölauahaldur välja</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="183"/>
        <source>Open desktop preference dialog on the page with the specified name</source>
        <translation>Ava määratud nimega lehel töölaua eelistuste aken</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="183"/>
        <location filename="../application.cpp" line="198"/>
        <source>NAME</source>
        <translation>NIMI</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="186"/>
        <source>Open new window</source>
        <translation>Ava uues aknas</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="189"/>
        <source>Open Find Files utility</source>
        <translation>Ava failiotsing</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="192"/>
        <source>Set desktop wallpaper from image FILE</source>
        <translation>Määra taustapildiks pildifail</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="192"/>
        <source>FILE</source>
        <translation>FAIL</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="195"/>
        <source>MODE</source>
        <translation>REŽIIM</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="195"/>
        <source>Set mode of desktop wallpaper. MODE=(%1)</source>
        <translation>Määra taustapildi režiim. REŽIIM=(%1)</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="198"/>
        <source>Open Preferences dialog on the page with the specified name</source>
        <translation>Ava määratud nimega lehel eelistuste aken</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="201"/>
        <source>Files or directories to open</source>
        <translation>Avatavad failid või kaustad</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="201"/>
        <source>[FILE1, FILE2,...]</source>
        <translation>[FAIL1, FAIL2,...]</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="627"/>
        <location filename="../application.cpp" line="632"/>
        <source>Error</source>
        <translation>Viga</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="632"/>
        <source>Terminal emulator is not set.</source>
        <translation>Terminalimulaatorit pole määratud.</translation>
    </message>
</context>
<context>
    <name>PCManFM::AutoRunDialog</name>
    <message>
        <location filename="../autorundialog.cpp" line="44"/>
        <source>Open in file manager</source>
        <translation>Ava failihalduris</translation>
    </message>
    <message>
        <location filename="../autorundialog.cpp" line="138"/>
        <source>Removable Disk</source>
        <translation>Eemaldatav ketas</translation>
    </message>
</context>
<context>
    <name>PCManFM::ConnectServerDialog</name>
    <message>
        <location filename="../connectserverdialog.cpp" line="9"/>
        <source>SSH</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../connectserverdialog.cpp" line="10"/>
        <source>FTP</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../connectserverdialog.cpp" line="11"/>
        <source>WebDav</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../connectserverdialog.cpp" line="12"/>
        <source>Secure WebDav</source>
        <translation>Turvaline WebDAV (HTTPS)</translation>
    </message>
    <message>
        <location filename="../connectserverdialog.cpp" line="13"/>
        <source>HTTP</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../connectserverdialog.cpp" line="14"/>
        <source>HTTPS</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>PCManFM::DesktopEntryDialog</name>
    <message>
        <location filename="../desktopentrydialog.cpp" line="29"/>
        <source>Command:</source>
        <translation>Käsk:</translation>
    </message>
    <message>
        <location filename="../desktopentrydialog.cpp" line="32"/>
        <source>URL:</source>
        <translation>URL:</translation>
    </message>
    <message>
        <location filename="../desktopentrydialog.cpp" line="57"/>
        <source>Select an icon</source>
        <translation>Vali ikoon</translation>
    </message>
    <message>
        <location filename="../desktopentrydialog.cpp" line="59"/>
        <source>Images (*.png *.xpm *.svg *.svgz )</source>
        <translation>Pildid (*.png *.xpm *.svg *.svgz )</translation>
    </message>
    <message>
        <location filename="../desktopentrydialog.cpp" line="81"/>
        <source>Select an executable file</source>
        <translation>Vali käivitusfail</translation>
    </message>
    <message>
        <location filename="../desktopentrydialog.cpp" line="89"/>
        <source>Select a file</source>
        <translation>Vali fail</translation>
    </message>
</context>
<context>
    <name>PCManFM::DesktopPreferencesDialog</name>
    <message>
        <location filename="../desktoppreferencesdialog.cpp" line="53"/>
        <source>Fill with background color only</source>
        <translation>Täida ainult taustavärviga</translation>
    </message>
    <message>
        <location filename="../desktoppreferencesdialog.cpp" line="54"/>
        <source>Stretch to fill the entire screen</source>
        <translation>Venita täitmaks kogu ekraani</translation>
    </message>
    <message>
        <location filename="../desktoppreferencesdialog.cpp" line="55"/>
        <source>Stretch to fit the screen</source>
        <translation>Venita sobitumaks ekraanile</translation>
    </message>
    <message>
        <location filename="../desktoppreferencesdialog.cpp" line="56"/>
        <source>Center on the screen</source>
        <translation>Ekraani keskele</translation>
    </message>
    <message>
        <location filename="../desktoppreferencesdialog.cpp" line="57"/>
        <source>Tile the image to fill the entire screen</source>
        <translation>Paljunda pilti täitmaks kogu ekraani</translation>
    </message>
    <message>
        <location filename="../desktoppreferencesdialog.cpp" line="58"/>
        <source>Zoom the image to fill the entire screen</source>
        <translation>Suurenda pilti täitmaks kogu ekraani</translation>
    </message>
    <message>
        <location filename="../desktoppreferencesdialog.cpp" line="255"/>
        <source>Select Wallpaper</source>
        <translation>Vali taustapilt</translation>
    </message>
    <message>
        <location filename="../desktoppreferencesdialog.cpp" line="259"/>
        <source>Image Files</source>
        <translation>Pildifailid</translation>
    </message>
    <message>
        <location filename="../desktoppreferencesdialog.cpp" line="301"/>
        <source>Select Wallpaper Folder</source>
        <translation>Vali taustapiltide kaust</translation>
    </message>
</context>
<context>
    <name>PCManFM::DesktopWindow</name>
    <message>
        <location filename="../desktopwindow.cpp" line="299"/>
        <source>Trash (One item)</source>
        <translation>Prügikast (üks element)</translation>
    </message>
    <message numerus="yes">
        <location filename="../desktopwindow.cpp" line="302"/>
        <source>Trash (%Ln items)</source>
        <translation>
            <numerusform>Prügikast (% Ln element)</numerusform>
            <numerusform>Prügikast (% Ln elementi)</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../desktopwindow.cpp" line="306"/>
        <source>Trash (Empty)</source>
        <translation>Prügikast (tühi)</translation>
    </message>
    <message>
        <location filename="../desktopwindow.cpp" line="341"/>
        <source>Computer</source>
        <translation>Arvuti</translation>
    </message>
    <message>
        <location filename="../desktopwindow.cpp" line="355"/>
        <source>Network</source>
        <translation>Võrk</translation>
    </message>
    <message>
        <location filename="../desktopwindow.cpp" line="963"/>
        <source>Open</source>
        <translation>Ava</translation>
    </message>
    <message>
        <location filename="../desktopwindow.cpp" line="969"/>
        <location filename="../desktopwindow.cpp" line="1009"/>
        <source>Stic&amp;k to Current Position</source>
        <translation>Jäta&amp; praegusele asukohale</translation>
    </message>
    <message>
        <location filename="../desktopwindow.cpp" line="977"/>
        <source>Empty Trash</source>
        <translation>Tühjenda prügikast</translation>
    </message>
    <message>
        <location filename="../desktopwindow.cpp" line="1036"/>
        <source>Hide Desktop Items</source>
        <translation>Peida töölauakirjed</translation>
    </message>
    <message>
        <location filename="../desktopwindow.cpp" line="1042"/>
        <source>Create Launcher</source>
        <translation>Loo käivitaja</translation>
    </message>
    <message>
        <location filename="../desktopwindow.cpp" line="1045"/>
        <source>Desktop Preferences</source>
        <translation>Töölaua eelistused</translation>
    </message>
</context>
<context>
    <name>PCManFM::FilterBar</name>
    <message>
        <location filename="../tabpage.cpp" line="93"/>
        <source>Filter:</source>
        <translation>Filter:</translation>
    </message>
</context>
<context>
    <name>PCManFM::FilterEdit</name>
    <message>
        <location filename="../tabpage.cpp" line="63"/>
        <source>Clear text (Ctrl+K or Esc)</source>
        <translation>Kustuta tekst (Ctrl+K või Esc)</translation>
    </message>
</context>
<context>
    <name>PCManFM::MainWindow</name>
    <message>
        <location filename="../mainwindow.cpp" line="141"/>
        <source>Root Instance</source>
        <translation>Juurkasutaja vaade</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="724"/>
        <source>Hide menu bar</source>
        <translation>Peida menüüriba</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="725"/>
        <source>This will hide the menu bar completely, use Ctrl+M to show it again.</source>
        <translation>See peidab menüüriba täiesti, kasuta Ctrl + M selle uuesti kuvamiseks.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1097"/>
        <source>Version: %1</source>
        <translation>Versioon: %1</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="174"/>
        <location filename="../mainwindow.cpp" line="2057"/>
        <source>&amp;Move to Trash</source>
        <translation>&amp;Liiguta prügikasti</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="174"/>
        <location filename="../mainwindow.cpp" line="2057"/>
        <source>&amp;Delete</source>
        <translation>&amp;Kustuta</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1900"/>
        <source>Customized View Settings</source>
        <translation>Vaate kohandatud seadistused</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2125"/>
        <location filename="../mainwindow.cpp" line="2133"/>
        <source>Error</source>
        <translation>Viga</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2133"/>
        <source>Switch user command is not set.</source>
        <translation>Kasutaja vahetamise käsku pole määratud.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2235"/>
        <source>Cleaning Folder Settings</source>
        <translation>Kustutan kausta seadistused</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2236"/>
        <source>Do you want to remove settings of nonexistent folders?
They might be useful if those folders are created again.</source>
        <translation>Kas sa soovid eemaldada olematute kaustade seadistused?
Kui need kaustad hiljem uuesti tekivad, siis võib-olla pole mõtet seda teha.</translation>
    </message>
</context>
<context>
    <name>PCManFM::PreferencesDialog</name>
    <message>
        <location filename="../preferencesdialog.cpp" line="199"/>
        <source>Icon View</source>
        <translation>Ikoonivaade</translation>
    </message>
    <message>
        <location filename="../preferencesdialog.cpp" line="200"/>
        <source>Compact View</source>
        <translation>Kompaktne vaade</translation>
    </message>
    <message>
        <location filename="../preferencesdialog.cpp" line="201"/>
        <source>Thumbnail View</source>
        <translation>Pisipildivaade</translation>
    </message>
    <message>
        <location filename="../preferencesdialog.cpp" line="202"/>
        <source>Detailed List View</source>
        <translation>Üksikasjalik nimekiri</translation>
    </message>
</context>
<context>
    <name>PCManFM::TabPage</name>
    <message>
        <location filename="../tabpage.cpp" line="506"/>
        <source>Error</source>
        <translation>Viga</translation>
    </message>
    <message>
        <location filename="../tabpage.cpp" line="515"/>
        <source>Free space: %1 (Total: %2)</source>
        <translation>Vaba ruum:%1 (kokku:%2)</translation>
    </message>
    <message numerus="yes">
        <location filename="../tabpage.cpp" line="532"/>
        <source>%n item(s)</source>
        <translation>
            <numerusform>%n kirje</numerusform>
            <numerusform>%n kirjet</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../tabpage.cpp" line="534"/>
        <source> (%n hidden)</source>
        <translation>
            <numerusform> (%n peidetud)</numerusform>
            <numerusform> (%n peidetud)</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../tabpage.cpp" line="539"/>
        <location filename="../tabpage.cpp" line="735"/>
        <location filename="../tabpage.cpp" line="749"/>
        <source>Link to</source>
        <translation>Ühenda</translation>
    </message>
    <message numerus="yes">
        <location filename="../tabpage.cpp" line="761"/>
        <source>%n item(s) selected</source>
        <translation>
            <numerusform>%n kirje valitud</numerusform>
            <numerusform>%n kirjet valitud</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>PCManFM::View</name>
    <message>
        <location filename="../view.cpp" line="59"/>
        <source>Many files</source>
        <translation>Palju faile</translation>
    </message>
    <message numerus="yes">
        <location filename="../view.cpp" line="60"/>
        <source>Do you want to open these %1 files?</source>
        <translation>
            <numerusform>Kas sa soovid avada selle %1 faili?</numerusform>
            <numerusform>Kas sa soovid avada need %1 faili?</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../view.cpp" line="122"/>
        <source>Open in New T&amp;ab</source>
        <translation>Ava uuel &amp;kaardil</translation>
    </message>
    <message>
        <location filename="../view.cpp" line="126"/>
        <source>Open in New Win&amp;dow</source>
        <translation>Ava uues &amp;aknas</translation>
    </message>
    <message>
        <location filename="../view.cpp" line="134"/>
        <source>Open in Termina&amp;l</source>
        <translation>Ava uues termina&amp;lis</translation>
    </message>
</context>
<context>
    <name>PreferencesDialog</name>
    <message>
        <location filename="../preferences.ui" line="14"/>
        <source>Preferences</source>
        <translation>Eelistused</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="45"/>
        <source>User Interface</source>
        <translation>Kasutajaliides</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="35"/>
        <source>Behavior</source>
        <translation>Käitumine</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="50"/>
        <location filename="../preferences.ui" line="605"/>
        <source>Thumbnail</source>
        <translation>Pisipilt</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="55"/>
        <source>Volume</source>
        <translation>Maht</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="60"/>
        <source>Advanced</source>
        <translation>Täpsem</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="222"/>
        <source>Select newly created files</source>
        <translation>Valige äsja loodud failid</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="259"/>
        <source>Icons</source>
        <translation>Ikoonid</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="291"/>
        <source>Size of big icons:</source>
        <translation>Suurte ikoonide suurus:</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="308"/>
        <source>Size of small icons:</source>
        <translation>Väikeste ikoonide suurus:</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="325"/>
        <source>Size of thumbnails:</source>
        <translation>Pisipiltide suurus:</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="339"/>
        <source>Size of side pane icons:</source>
        <translation>Külgpaneeli ikoonide suurus:</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="271"/>
        <source>Icon theme:</source>
        <translation>Ikooniteema:</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="509"/>
        <source>Window</source>
        <translation>Aken</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="522"/>
        <source>Always show the tab bar</source>
        <translation>Kaardiriba on alati nähtaval</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="529"/>
        <source>Show &apos;Close&apos; buttons on tabs	</source>
        <translation>Kuva kaartidel sulgemisnuppu	</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="543"/>
        <source>Remember the size of the last closed window</source>
        <translation>Pea viimati suletud akna suurus meeles</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="550"/>
        <source>Default width of new windows:</source>
        <translation>Uute akende laius vaikimisi:</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="564"/>
        <source>Default height of new windows:</source>
        <translation>Uute akende kõrgus vaikimisi:</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="81"/>
        <source>Browsing</source>
        <translation>Sirvimine</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="93"/>
        <source>Open files with single click</source>
        <translation>Ava failid ühe klõpsuga</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="113"/>
        <source>Default view mode:</source>
        <translation>Vaikevaate režiim:</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="129"/>
        <source> sec</source>
        <translation> sek</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="175"/>
        <source>File Operations</source>
        <translation>Failioperatsioonid</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="181"/>
        <source>Confirm before deleting files</source>
        <translation>Kinnitus enne failide kustutamist</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="188"/>
        <source>Move deleted files to &quot;trash bin&quot; instead of erasing from disk.</source>
        <translation>Liiguta kustutatud failid prügikasti nende kettalt eemaldamise asemel.</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="614"/>
        <source>Show thumbnails of files</source>
        <translation>Kuva failide pisipildid</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="621"/>
        <source>Only show thumbnails for local files</source>
        <translation>Kuva ainult kohalike failide pisipildid</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="40"/>
        <source>Display</source>
        <translation>Ekraan</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="90"/>
        <source>If this is unchecked, the DE setting will be used.</source>
        <translation>Kui see valik on tegemata, siis kasutame töölauakeskonna seadistusi.</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="103"/>
        <location filename="../preferences.ui" line="126"/>
        <source>Set to zero to disable auto-selection.</source>
        <translation>Automaatse valimise väljalülitamiseks kirjuta väärtuseks 0.</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="106"/>
        <source>Delay of auto-selection in single click mode:</source>
        <translation>Viivitus automaatsel valimisel ühe klõpsu režiimis:</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="147"/>
        <source>Open in current tab</source>
        <translation>Ava aktiivsel kaardil</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="152"/>
        <source>Open in new tab</source>
        <translation>Ava uuel kaardil</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="157"/>
        <source>Open in new window</source>
        <translation>Ava uues aknas</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="195"/>
        <source>Erase files on removable media instead of &quot;trash can&quot; creation</source>
        <translation>Kustuta failid eemaldataval andmekandjal prügikasti loomise asemel</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="202"/>
        <source>Confirm before moving files into &quot;trash can&quot;</source>
        <translation>Kinnitus enne failide prügikasti saatmist</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="209"/>
        <location filename="../preferences.ui" line="372"/>
        <location filename="../preferences.ui" line="382"/>
        <source>Requires application restart to take effect completely</source>
        <translation>Nõuab rakenduse taaskäivitamist, et see täielikult jõustuda</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="212"/>
        <source>Launch executable files without prompt</source>
        <translation>Käivita käivitatavaid faile ilma küsimata</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="219"/>
        <source>Renamed files will also be selected</source>
        <translation>Vali ka ümbernimetatud failid</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="288"/>
        <location filename="../preferences.ui" line="298"/>
        <source>Used by Icon View</source>
        <translation>Kasutatakse ikoonivaates</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="305"/>
        <location filename="../preferences.ui" line="315"/>
        <source>Used by Compact View and Detailed List View</source>
        <translation>Kasutatakse kompaktses ja üksikasjaliku nimekirja vaates</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="322"/>
        <location filename="../preferences.ui" line="332"/>
        <source>Used by Thumbnail View</source>
        <translation>Kasutatakse pisipildivaates</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="352"/>
        <source>User interface</source>
        <translation>Kasutajaliides</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="365"/>
        <source>Treat backup files as hidden</source>
        <translation>Varundatud faile koheldakse peidetuina</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="385"/>
        <source>Show icons of hidden files shadowed</source>
        <translation>Kuva peidetud failide ikoonid varjutatuna</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="399"/>
        <source>Disable smooth scrolling in list and compact modes</source>
        <translation>Kompaktses ja üksikasjalikus vaates ära kasuta sujuvat kerimist</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="422"/>
        <source>Minimum item margins in icon view:</source>
        <translation>Vähim kirjete vahemaa ikoonivaates:</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="429"/>
        <source>3 px by default.</source>
        <translation>Vaikimisi 3 px.</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="432"/>
        <location filename="../preferences.ui" line="456"/>
        <source> px</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="445"/>
        <source>x</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="452"/>
        <source>3 px by default.
A space is also reserved for 3 lines of text.</source>
        <translation>Vaikimisi 3 px.
Ruumi varuti ka 3 tekstirea jaoks.</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="469"/>
        <source>Lock</source>
        <translation>Lukusta</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="518"/>
        <source>When unchecked, the tab bar will be shown
only if there are more than one tab.</source>
        <translation>Kui see pole märgitud, näidatakse kaardiriba ainult siis, kui avatud on rohkem kui üks kaart.</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="536"/>
        <source>Switch to newly opened tab</source>
        <translation>Säti fookus viimati avatud kaardile</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="578"/>
        <source>Reopen last window tabs in a new window</source>
        <translation>Uues aknas ava viimati avatud olnud kaardid</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="628"/>
        <location filename="../preferences.ui" line="640"/>
        <source>The built-in thumbnailer makes thumbnails of images that are supported by Qt.

Usually, most image types are supported. The default size limit is 4 MiB.</source>
        <translation>Lõimitud pisipildistaja teeb pisipilte kõikidest pildifailidest, mida Qt suudab lugeda.

Enamus tavalisi pildifaile on seega toetatud. Vaikimisi failisuurus on 4 MiB.</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="633"/>
        <source>Image size limit for built-in thumbnailer:</source>
        <translation>Pisipildistaja failisuuruse ülempiir:</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="645"/>
        <location filename="../preferences.ui" line="681"/>
        <source> MiB</source>
        <translation> MiB</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="661"/>
        <location filename="../preferences.ui" line="673"/>
        <source>If existing, external thumbnailers are used for videos, PDF documents, etc.

A value of -1 means that there is no limit for the file size (the default).</source>
        <translation>Kui nad on olemas, siis kasutame väliseid pisipildistajaid videote, pdf-failide ja muude sarnaste failide jaoks.

Väärtus -1 tähendab, et failisuuruse piir puudub (see on ka vaikimisi väärtus).</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="666"/>
        <source>File size limit for external thumbnailers:</source>
        <translation>Välise pisipildistaja failisuurus:</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="678"/>
        <source>No limit</source>
        <translation>Piir puudub</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="816"/>
        <source>Examples:&lt;br&gt;For terminal: &lt;i&gt;xterm -e %s&lt;/i&gt;&lt;br&gt;For switching user: &lt;i&gt;lxsudo %s&lt;/i&gt; or &lt;i&gt;lxsudo dbus-run-session -- %s&lt;/i&gt;&lt;br&gt;&lt;i&gt;%s&lt;/i&gt; is the command line you want to execute with terminal or su.&lt;br&gt; Important: Please use lxsudo, sudo alone will wreck permissions of the settings file.</source>
        <translation>Näited:&lt;br&gt;Terminali jaoks: &lt;i&gt;xterm -e %s&lt;/i&gt;&lt;br&gt;Kasutaja vahetamiseks: &lt;i&gt;lxsudo %s&lt;/i&gt; või &lt;i&gt;lxsudo dbus-run-session -- %s&lt;/i&gt;&lt;br&gt;&lt;i&gt;%s&lt;/i&gt; on käsurea näited, mida saad kasutada terminalis.&lt;br&gt; Oluline: Kuna ainult sudo kasutamine tekitab kaose seadistuste faili õigustes, siis palun kasuta rakendust lxsudo.</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="907"/>
        <source>Application restart is needed for changes to take effect.</source>
        <translation>Selle muudatuse jõustamiseks pead rakenduse uuesti käivitama.</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="139"/>
        <source>Bookmarks menu:</source>
        <translation>Järjehoidjate menüü:</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="165"/>
        <source>Show folder context menu with Ctrl + right click</source>
        <translation>Näita kaustade kontekstimenüüd Ctrl + hiire parema klahvi klõpsamisel</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="229"/>
        <source>Open folders in new tabs as far as possible</source>
        <translation>Ava kaustad uute kaartidena nii kaugel kui võimalik</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="232"/>
        <source>Single window mode</source>
        <translation>Kasuta ühe akna režiimi</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="375"/>
        <source>Always show real file names</source>
        <translation>Alati näita tegelikke failinimesid</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="392"/>
        <source>Do not show file tooltips</source>
        <translation>Ära näita failide teabemulle</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="611"/>
        <source>Needs ffmpegthumbnailer</source>
        <translation>Vajalik on ffmpegthumbnailer</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="717"/>
        <source>Auto Mount</source>
        <translation>Automaatne haakimine</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="723"/>
        <source>Mount mountable volumes automatically on program startup</source>
        <translation>Haagi seadmed automaatselt programmi käivitumisel</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="730"/>
        <source>Mount removable media automatically when they are inserted</source>
        <translation>Haagi eemaldatavad andmekandjad automaatselt ühendamisel masinaga</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="737"/>
        <source>Show available options for removable media when they are inserted</source>
        <translation>Kuva eemaldatava andmekandja ühendamisel saadaolevad valikud automaatselt</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="747"/>
        <source>When removable medium unmounted:</source>
        <translation>Kui eemaldatav andmekandja on lahti ühendatud:</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="753"/>
        <source>Close &amp;tab containing removable medium</source>
        <translation>Sulge eemaldatavat kandjat sisaldav &amp;kaart</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="760"/>
        <source>Chan&amp;ge folder in the tab to home folder</source>
        <translation>Muud&amp;a kaardil olev kaust kodukaustaks</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="787"/>
        <source>Programs</source>
        <translation>Programmid</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="796"/>
        <source>Terminal emulator:</source>
        <translation>Terminaliemulaator:</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="803"/>
        <source>Switch &amp;user command:</source>
        <translation>&amp;Kasutaja vahetamise käsk:</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="829"/>
        <source>Archiver in&amp;tegration:</source>
        <translation>Pakkija in&amp;tegreerimine:</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="852"/>
        <source>Templates</source>
        <translation>Mallid</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="858"/>
        <source>Show only user defined templates in menu</source>
        <translation>Kuva menüüs ainult kasutaja määratud mallid</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="865"/>
        <source>Show only one template for each MIME type</source>
        <translation>Kuva iga MIME tüübi kohta ainult ühte malli</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="872"/>
        <source>Run default application after creation from template</source>
        <translation>Mallist loomise järel käivita vaikimisi rakendus</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="358"/>
        <source>Use SI decimal prefixes instead of IEC binary prefixes</source>
        <translation>IEC-i binaarseteks eesliidete asemel kasuta SI-eelistusi</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../bulkrename.cpp" line="73"/>
        <source>Renaming files...</source>
        <translation>Muudan failide nimesid...</translation>
    </message>
    <message>
        <location filename="../bulkrename.cpp" line="73"/>
        <source>Abort</source>
        <translation>Katkesta</translation>
    </message>
    <message>
        <location filename="../bulkrename.cpp" line="82"/>
        <source>Warning</source>
        <translation>Hoiatus</translation>
    </message>
    <message>
        <location filename="../bulkrename.cpp" line="82"/>
        <source>Renaming is aborted.</source>
        <translation>Ümbernimetamine katkeb.</translation>
    </message>
    <message>
        <location filename="../bulkrename.cpp" line="108"/>
        <location filename="../bulkrename.cpp" line="111"/>
        <location filename="../tabpage.cpp" line="1077"/>
        <source>Error</source>
        <translation>Viga</translation>
    </message>
    <message>
        <location filename="../bulkrename.cpp" line="108"/>
        <source>No file could be renamed.</source>
        <translation>Ühtki faili ei nimetatud ümber.</translation>
    </message>
    <message>
        <location filename="../bulkrename.cpp" line="111"/>
        <source>Some files could not be renamed.</source>
        <translation>Mõningaid faile ei saanud ümber nimetada.</translation>
    </message>
    <message>
        <location filename="../tabpage.cpp" line="1077"/>
        <source>Cannot open as Admin.</source>
        <translation>Ei saa avada administraatorina.</translation>
    </message>
</context>
</TS>
